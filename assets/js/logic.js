'use strict';


let socket = io.connect("http://localhost:8000/oboseen");
attachEventListeners(socket);
let pendingReports = 0;
let reportId;

$('#approve').click(function (event) {
	event.preventDefault();
	if (typeof(reportId) !== 'undefined') {
		const remarks = $('#remarks').val();
		validateReport({
			reportId: reportId,
			isValid: true,
			remarks: remarks
		});
	}
});

$('#reject').click(function (event) {
	event.preventDefault();
	if (typeof(reportId) !== 'undefined') {
		validateReport({
			reportId: reportId,
			isValid: false,
			remarks: 'No incident'
		});
	}
})

function validateReport (review) {
	socket.emit('validate', {
		report_id: review.reportId,
		is_valid: review.isValid,
		remarks: review.remarks
	}, function (data) {
		if (data.error) {
			console.log(data.error);
		} else {
			if (review.isValid) {	
				demo.showNotification();
			} else {
				demo.showNotificationReject();
			}
			$('#row-' + review.reportId).remove();
			$('#remarks').val('');
			setVideoUrl('');
			pendingReports--;
			$('#pending-count').html(pendingReports);
			if (!pendingReports) {
				$('.placeholder').show();
				$('table').hide();
			}
		}
	});
}


function attachEventListeners(socket) {
	socket.on('connect', function() {
		socket.emit('log-in', {role: 'reviewer'});
	})

	socket.on('receive report', function(data) {
		addReportRow (data);
		pendingReports++;
		$('#pending-count').html(pendingReports);
		$('.placeholder').hide();
		$('table').show();
	});
}

function addReportRow (report) {
	const newRow = $(
		'<tr id="row-' + report.report_id + '">' +
		'<td>' + report.reporter_name + '</td>' +
		'<td>' + report.report_address + '</td>' +
		'<td>' + report.report_date + '</td>' +
		'<td id="watch-' + report.report_id + '"></td>' + 
		'</tr>');
	
	const watchButton = $('<a href="#" class="btn btn-primary btn-round">Watch</a>');
	watchButton.click (function() {
		reportId = report.report_id;
		setVideoUrl(report.report_recording_url);
	});
	newRow.find('#watch-' + report.report_id).append(watchButton);
	$('tbody').append(newRow);
}

function setVideoUrl (url) {
	if (url.length > 0) {
		$('#video-card').show();
	} else $('#video-card').hide();
	$('iframe')[0].src = url;
}